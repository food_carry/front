import axios from "axios";

let headers = {
  Accept: "application/json",
};
export default axios.create({
  baseURL: "https://www.foodcarryapi.m1studios.ru/",
  //baseURL: "http://localhost:8000",
  timeout: 1000,
  headers,
});
