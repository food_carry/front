import { createStore } from "vuex";
import dishes from "@/store/modules/dishes.js";
import order from "@/store/modules/order.js";
import user from "@/store/modules/user";

export default createStore({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: { dishes, order, user },
});
