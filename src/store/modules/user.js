import api from "@/api";

const state = {
  userTgData: {
    tg_id: localStorage.getItem("tg_id") || "",
  },
};
const getters = {};
const mutations = {
  SET_USER_DATA: (state, userTgData) => {
    console.log(userTgData);
    state.userTgData = { ...userTgData };
    localStorage.setItem("tg_id", userTgData.tg_id);
  },
};
const actions = {
  tg_login: async ({ commit }, userTgData) => {
    userTgData.tg_id = userTgData.id;
    delete userTgData.id;
    api
      .post("tg/", userTgData)
      .then(() => {
        commit("SET_USER_DATA", userTgData);
      })
      .catch((err) => console.log(err));
  },
  get_user: async ({ commit }, tg_id) => {
    api
      .get("tg/", { params: { tg_id } })
      .then((response) => {
        console.log(response.data);
        commit("SET_USER_DATA", response.data);
      })
      .catch((err) => console.log(err));
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
