import api from "@/api";
//TODO: Добавить обработку ошибок
const state = {
  categories: [],
  dishes: [],
};
// Добавить
const getters = {
  // Получить категории блюда
  GET_CATEGORIES_FOR_DISH: (state) => (dish) => {
    return state.categories.filter((category) =>
      dish.categories?.includes(category.id)
    );
  },
  GET_DISHES_BY_CATEGORY_ID: (state) => (category_id) => {
    return state.dishes.filter((dish) => {
      return dish.categories.indexOf(+category_id) !== -1;
    });
  },
  GET_CATEGORIES: (state) => state.categories,
  GET_DISHES: (state) => {
    const dishes = {};
    state.dishes.forEach((dish) => (dishes[dish.id] = dish));
    return dishes
  },
};
const mutations = {
  SET_CATEGORIES(state, categories) {
    state.categories = categories;
  },
  SET_DISHES(state, dishes) {
    state.dishes = dishes;
  },
  RESET_DISHES(state) {
    state.dishes = [];
  },
};
const actions = {
  // Получить категории
  get_categories: async ({ commit }) => {
    api
      .get("/api/v1/category/")
      .then((response) => commit("SET_CATEGORIES", response.data))
      .catch((err) => {
        console.log(err);
      });
  },
  // Получить блюда по категории
  get_dishes: async ({ commit }) => {
    // TODO: Сделать поддержку разных заведений
    api
      .get(`/api/v1/catering/1/dishes/`, { params: { is_stopped: false } })
      .then((response) => commit("SET_DISHES", response.data));
  },
  // Сбросить список блюд
  reset_dishes: ({ commit }) => {
    commit("RESET_DISHES");
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
