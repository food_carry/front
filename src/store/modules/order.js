//import api from "@/api";

import api from "@/api";

const state = {
  cart: {},
  total_price: 0,
  client_info: {
    first_name: "",
    second_name: "",
    client_email: "",
    comment: "",
    paid_type: "",
    delivery_time: null,
  },
  order_id: null,
};
const getters = {
  // TODO: Убрать лишние геттеры
  GET_CART: (state) => state.cart,
  GET_TOTAL_PRICE: (state) => state.total_price,
  GET_CLIENT_INFO: (state) => state.client_info,
};
const mutations = {
  ADD_DISH_TO_CART(state, { dish_id, count }) {
    if (Object.keys(state.cart).indexOf(dish_id.toString()) === -1) {
      state.cart[dish_id] = count;
    } else {
      state.cart[dish_id] += count;
    }
  },
  REMOVE_DISH_FROM_CART(state, dish_id) {
    delete state.cart[dish_id];
  },
  RESET_CART(state) {
    state.cart = {};
  },
  SET_ORDER_ID(state, order_id) {
    state.order_id = order_id;
  },
  SET_ORDER_DISH_COUNT(state, { dish_id, count }) {
    state.cart[dish_id] = count;
  },
};
const actions = {
  add_dish_to_cart: async ({ commit }, { dish_id, count }) => {
    commit("ADD_DISH_TO_CART", { dish_id, count });
  },
  remove_dish_from_cart: async ({ commit }, dish_id) => {
    commit("REMOVE_DISH_FROM_CART", dish_id);
  },
  reset_cart: async ({ commit }) => {
    commit("RESET_CART");
  },
  set_order_dish_count({ commit }, { dish_id, count }) {
    commit("SET_ORDER_DISH_COUNT", { dish_id, count });
  },
  create_order: async ({ commit, getters, dispatch }, { catering, tg_id, delivery_time = null }) => {
    const order_data = getters.GET_CLIENT_INFO;
    order_data.catering = catering;
    order_data.tg_user = tg_id;
    console.log(delivery_time)
    if (delivery_time) {
      order_data.delivery_time = delivery_time;
    }
    return api.post("/api/v1/order/", order_data).then((response) => {
      commit("SET_ORDER_ID", response.data.id);
      dispatch("set_order_dishes", response.data.id);
    });
  },
  set_order_dishes: async ({ getters }, order_id) => {
    api
      .post(`/api/v1/order/${order_id}/dishes/`, getters.GET_CART)
      .catch((err) => console.log(err));
  },
  set_order_id({ commit }, order_id) {
    commit("SET_ORDER_ID", order_id);
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
