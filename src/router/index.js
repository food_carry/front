import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/catering/:catering",
    name: "home",
    component: () => import("@/views/HomeView.vue"),
  },
  {
    path: "/catering/:catering/category/:category",
    name: "category",
    component: () => import("@/views/CategoryView.vue"),
  },
  {
    path: "/catering/:catering/cart",
    name: "cart",
    component: () => import("@/views/CartView.vue"),
  },
  /*{
    path: "/catering/:catering/order",
    name: "order",
    component: () => import("@/views/OrderInfoView.vue"),
  },*/
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/LoginView.vue"),
  },
  {
    path: "/error-404",
    name: "error-404",
    component: () => import("@/views/error/TheError404.vue"),
    meta: {
      layout: "full",
    },
  },
  // TODO: Переход на заведение 1
  {
    path: "/",
    redirect: "/login",
  },
  {
    path: "/:pathMatch(.*)*",
    redirect: "/login",
  },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
